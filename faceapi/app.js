async function runModel(image) {
    await faceapi.nets.ssdMobilenetv1.loadFromUri('/faceapi/models');
    await faceapi.nets.faceLandmark68Net.loadFromUri('/faceapi/models');
    await faceapi.nets.faceExpressionNet.loadFromUri('/faceapi/models');
    const canvas = faceapi.createCanvasFromMedia(image);
    const detection = await faceapi.detectAllFaces(image)
                                    .withFaceLandmarks()
                                    .withFaceExpressions();

    const dimensions = {
        width: image.width,
        height: image.height
    };
    
    const resizedDimensions = faceapi.resizeResults(detection, dimensions);
    
    document.body.append(canvas);
    //console.log(detection);
    faceapi.draw.drawDetections(canvas, resizedDimensions);
    faceapi.draw.drawFaceLandmarks(canvas, resizedDimensions);
    faceapi.draw.drawFaceExpressions(canvas, resizedDimensions);
    return (detection[0].expressions)
}